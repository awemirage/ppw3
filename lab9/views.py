from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.urls import reverse
from . import forms, urls

# Create your views here.

def index(request):
    if request.user.is_authenticated:
        return render(request, 'lab9/index.html', {'user_login' : request.user.username})
    return render(request, 'lab9/index.html', {'user_login' : 'user'})

def register(request):
    if request.method == 'POST':
        form = forms.RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return render(request, 'lab9/index.html', {'user_login' : username})
    else:
        form = forms.RegisterForm()
    return render(request, 'lab9/register.html', {'form_register': form})