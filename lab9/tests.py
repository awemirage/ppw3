from django.test import TestCase
from . import forms, models

# Create your tests here.
class Lab9UnitTest(TestCase):
    #Test Url
    def test_index_url_exists(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exists(self):
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_register_url_exists(self):
        response = self.client.get('/register/')
        self.assertEqual(response.status_code, 200)

    #Test Template
    def test_index_template(self):
        response = self.client.get('')
        self.assertTemplateUsed(response, 'lab9/index.html')

    def test_login_me_template(self):
        response = self.client.get('/accounts/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
    
    def test_contact_me_template(self):
        response = self.client.get('/register/')
        self.assertTemplateUsed(response, 'lab9/register.html')

    def test_register_forms(self):
        form_data = {
            'username': 'DemoName',
            'name' : 'DeDemo',
            'email': 'demo@demo.com',
            'password1' : 'Dekappa123',
            'password2' : "Dekappa123",
        }
        register_form = forms.RegisterForm(data=form_data)
        self.assertTrue(register_form.is_valid())

    #Test Views

    
