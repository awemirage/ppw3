from django.urls import path, include
from . import views

app_name='lab9'
urlpatterns = [
    path('', views.index, name="index"),
    path('register/', views.register, name="register"),
    path('accounts/', include('django.contrib.auth.urls')), 
]