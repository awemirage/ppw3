from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class RegisterForm(UserCreationForm):
    username_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'username',
        'placeholder':'Put your account name here',
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id':'name',
        'placeholder':'Put your name here',
    }
    email_attrs = {
        'type': 'email',
        'class': 'form-control',
        'id':'email',
        'placeholder':"Optional",
    }
    password1_attrs = {
        'type': 'password',
        'class': 'form-control',
        'id':'password1',
    }
    password2_attrs = {
        'type': 'password',
        'class': 'form-control',
        'id':'password2',
    }

    username = forms.CharField(max_length=30, widget=forms.TextInput(attrs=username_attrs))
    name = forms.CharField(label='Name ', max_length=30, required=True, widget=forms.TextInput(attrs=name_attrs))
    email = forms.CharField(label='Email ', max_length=30, required=False, widget=forms.TextInput(attrs=email_attrs))
    password1 = forms.CharField(label='Password ', widget=forms.PasswordInput(attrs=password1_attrs))
    password2 = forms.CharField(label='Password confirmation ', widget=forms.PasswordInput(attrs=password2_attrs), help_text= 'Enter the same password as above, for verification.')

    class Meta:
        model = User
        fields = ('username', 'name', 'email', 'password1', 'password2', )